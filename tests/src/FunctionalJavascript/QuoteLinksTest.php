<?php

namespace Drupal\Tests\quote\FunctionalJavascript;

use Drupal\comment\Entity\Comment;
use Drupal\comment\Tests\CommentTestTrait;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\editor\Entity\Editor;
use Drupal\filter\Entity\FilterFormat;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\user\Entity\User;
use Drupal\user\RoleInterface;

// cspell:ignore Lorem ipsum dolor sit amet, consectetur adipiscing elit ipsu
// cspell:ignore Duis in eros quam. Aliquam interdum, risus a molestie efficitur

/**
 * Tests quote links.
 *
 * @group quote
 */
class QuoteLinksTest extends WebDriverTestBase {

  use CommentTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'olivero';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'comment',
    'ckeditor5',
    'field_ui',
    'quote',
  ];

  /**
   * User with correct permissions.
   */
  protected User $user;

  /**
   * Config Factory.
   */
  protected ConfigFactoryInterface $config;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->drupalCreateContentType(['type' => 'article']);
    $this->addDefaultCommentField('node', 'article');

    $this->config = $this->container->get('config.factory');
    $this->config->getEditable('quote.settings')
      ->set('quote_allow_types', ['article'])
      ->save();
  }

  /**
   * Tests node links.
   */
  public function testNodeLinks(): void {
    $full_html_format = FilterFormat::create([
      'format' => 'full_html',
      'name' => 'Full HTML',
    ]);
    $full_html_format->save();

    $text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.';

    $node = $this->createNode([
      'title' => 'Test node',
      'type' => 'article',
      'uid' => 1,
      'body' => [
        'value' => '<p>' . $text . '</p>',
        'format' => 'full_html',
      ],
    ]);

    $this->user = $this->drupalCreateUser([
      'use quote',
      'access comments',
      'post comments',
      'skip comment approval',
      'access user profiles',
      $full_html_format->getPermissionName(),
    ]);

    $this->drupalLogin($this->user);
    $this->drupalGet('node/' . $node->id());
    $session = $this->getSession();
    $page = $this->getSession()->getPage();

    // Test "node quote all".
    $this->click('.node-quote-all-link');
    $page->findButton('edit-submit')->click();
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->elementExists('css', '#comment-1');
    $comment_test = $page->find('css', '#comment-1 .field--name-comment-body')->getHtml();
    $expected = "<blockquote><strong>admin wrote:</strong> {$text}</blockquote><p><br></p>";
    $this->assertSame($expected, $comment_test);

    // Test "node quote selected".
    $script = <<<EndOfScript
(function ($) {
  let content = $('.node--type-article .field--name-body').contents();
  let range = document.createRange();
  range.setStart(content[0].firstChild, 1);
  range.setEnd(content[0].firstChild, 10);
  let sel = document.getSelection();
  sel.removeAllRanges();
  sel.addRange(range);
})(jQuery);
EndOfScript;
    $session->evaluateScript($script);
    $this->click('.node-quote-sel-link');
    $page->findButton('edit-submit')->click();
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->elementExists('css', '#comment-2');
    $comment_test = $page->find('css', '#comment-2 .field--name-comment-body')->getHtml();
    $expected = "<blockquote><strong>admin wrote:</strong> orem ipsu</blockquote><p><br></p>";
    $this->assertSame($expected, $comment_test);
  }

  /**
   * Tests comment links.
   */
  public function testCommentLinks(): void {
    $full_html_format = FilterFormat::create([
      'format' => 'full_html',
      'name' => 'Full HTML',
    ]);
    $full_html_format->save();

    $node = $this->createNode([
      'title' => 'Test node',
      'type' => 'article',
      'uid' => 1,
      'body' => [
        'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        'format' => 'full_html',
      ],
    ]);

    $this->user = $this->drupalCreateUser([
      'use quote',
      'access comments',
      'post comments',
      'skip comment approval',
      'access user profiles',
      $full_html_format->getPermissionName(),
    ]);

    $this->drupalLogin($this->user);
    $this->drupalGet('node/' . $node->id());
    $session = $this->getSession();
    $page = $this->getSession()->getPage();

    // Write test comment.
    $text = 'Duis in eros quam. Aliquam interdum, risus a molestie efficitur.';
    $comment = Comment::create([
      'entity_type' => 'node',
      'entity_id' => $node->id(),
      'comment_type' => 'comment',
      'field_name' => 'comment',
      'pid' => 0,
      'uid' => 1,
      'status' => 1,
      'comment_body' => [
        ['value' => $text],
      ],
    ]);
    $comment->save();
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->elementExists('css', '#comment-1');

    // Test "comment quote all".
    $this->click('#comment-1 .comment-quote-all-link');
    $page->findButton('edit-submit')->click();
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->elementExists('css', '#comment-2');
    $comment_test = $page->find('css', '#comment-2 .field--name-comment-body')->getHtml();
    $comment_test = str_replace("\n", '', $comment_test);
    $expected = "<blockquote><strong>admin wrote:</strong> {$text}</blockquote><p><br></p>";
    $this->assertSame($expected, $comment_test);

    // Test "comment reply and quote all".
    $this->click('#comment-1 .comment-quote-all-reply-link');
    $page->findButton('edit-submit')->click();
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->elementExists('css', '#comment-3');
    $comment_test = $page->find('css', '#comment-3 .field--name-comment-body')->getHtml();
    $expected = "<blockquote><strong>admin wrote:</strong> {$text}</blockquote><p><br></p>";
    $this->assertSame($expected, $comment_test);

    // Test "comment quote selected".
    $script = <<<EndOfScript
(function ($) {
  let content = $('#comment-1 .field--name-comment-body').contents();
  let range = document.createRange();
  range.setStart(content[0].firstChild, 1);
  range.setEnd(content[0].firstChild, 10);
  let sel = document.getSelection();
  sel.removeAllRanges();
  sel.addRange(range);
})(jQuery);
EndOfScript;
    $session->evaluateScript($script);
    $this->click('.comment-quote-sel-link');
    $page->findButton('edit-submit')->click();
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->elementExists('css', '#comment-4');
    $comment_test = $page->find('css', '#comment-4 .field--name-comment-body')->getHtml();
    $expected = "<blockquote><strong>admin wrote:</strong> uis in er</blockquote><p><br></p>";
    $this->assertSame($expected, $comment_test);
  }

  /**
   * Tests CKEditor node links.
   */
  public function testCkeditorNodeLinks(): void {
    $full_html_format = FilterFormat::create(
      [
        'format' => 'full_html',
        'name' => 'Full HTML',
        'roles' => [RoleInterface::AUTHENTICATED_ID],
      ]
    );
    $full_html_format->save();

    Editor::create(
      [
        'format' => 'full_html',
        'editor' => 'ckeditor5',
      ]
    )->save();

    $this->config->getEditable('quote.settings')
      ->set('quote_ckeditor_support', TRUE)
      ->save();

    $text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.';

    $node = $this->createNode([
      'title' => 'Test node',
      'type' => 'article',
      'uid' => 1,
      'body' => [
        'value' => '<p>' . $text . '</p>',
        'format' => 'full_html',
      ],
    ]);

    $this->user = $this->drupalCreateUser([
      'use quote',
      'access comments',
      'post comments',
      'skip comment approval',
      'access user profiles',
      $full_html_format->getPermissionName(),
    ]);

    $this->drupalLogin($this->user);
    $this->drupalGet('node/' . $node->id());
    $session = $this->getSession();
    $page = $this->getSession()->getPage();

    // Test "node quote all".
    $this->assertSession()->elementExists('css', '.node-quote-all-link');
    $this->click('.node-quote-all-link');
    $this->assertSession()->elementExists('css', '#edit-submit');
    $page->findButton('edit-submit')->click();
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->elementExists('css', '#comment-1');

    $comment_test = $page->find('css', '#comment-1 .field--name-comment-body')->getHtml();
    $expected = "<blockquote><p><strong>admin wrote:</strong> {$text}</p></blockquote><p>&nbsp;</p>";
    $this->assertSame($expected, $comment_test);

    // Test "node quote selected".
    $script = <<<EndOfScript
(function ($) {
  let content = $('.node--type-article .field--name-body').contents();
  let range = document.createRange();
  range.setStart(content[0].firstChild, 1);
  range.setEnd(content[0].firstChild, 10);
  let sel = document.getSelection();
  sel.removeAllRanges();
  sel.addRange(range);
})(jQuery);
EndOfScript;
    $session->evaluateScript($script);
    $this->click('.node-quote-sel-link');
    $page->findButton('edit-submit')->click();
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->elementExists('css', '#comment-2');
    $comment_test = $page->find('css', '#comment-2 .field--name-comment-body')->getHtml();
    $expected = "<blockquote><p><strong>admin wrote:</strong> orem ipsu</p></blockquote><p>&nbsp;</p>";
    $this->assertSame($expected, $comment_test);
  }

  /**
   * Tests CKEditor comment links.
   */
  public function testCkeditorCommentLinks(): void {
    $full_html_format = FilterFormat::create(
      [
        'format' => 'full_html',
        'name' => 'Full HTML',
        'roles' => [RoleInterface::AUTHENTICATED_ID],
      ]
    );
    $full_html_format->save();

    Editor::create(
      [
        'format' => 'full_html',
        'editor' => 'ckeditor5',
      ]
    )->save();

    $this->config->getEditable('quote.settings')
      ->set('quote_ckeditor_support', TRUE)
      ->save();

    $node = $this->createNode([
      'title' => 'Test node',
      'type' => 'article',
      'uid' => 1,
      'body' => [
        'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        'format' => 'full_html',
      ],
    ]);

    $this->user = $this->drupalCreateUser([
      'use quote',
      'access comments',
      'post comments',
      'skip comment approval',
      'access user profiles',
      $full_html_format->getPermissionName(),
    ]);

    $this->drupalLogin($this->user);
    $this->drupalGet('node/' . $node->id());
    $session = $this->getSession();
    $page = $this->getSession()->getPage();

    // Write test comment.
    $text = 'Duis in eros quam. Aliquam interdum, risus a molestie efficitur.';
    $comment = Comment::create([
      'entity_type' => 'node',
      'entity_id' => $node->id(),
      'comment_type' => 'comment',
      'field_name' => 'comment',
      'pid' => 0,
      'uid' => 1,
      'status' => 1,
      'comment_body' => [
        ['value' => $text],
      ],
    ]);
    $comment->save();
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->elementExists('css', '#comment-1');

    // Test "comment quote all".
    $this->click('#comment-1 .comment-quote-all-link');
    $page->findButton('edit-submit')->click();
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->elementExists('css', '#comment-2');
    $comment_test = $page->find('css', '#comment-2 .field--name-comment-body')->getHtml();
    $expected = "<blockquote><p><strong>admin wrote:</strong> {$text}</p></blockquote><p>&nbsp;</p>";
    $this->assertSame($expected, $comment_test);

    // Test "comment reply and quote all".
    $this->click('#comment-1 .comment-quote-all-reply-link');
    $page->findButton('edit-submit')->click();
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->elementExists('css', '#comment-3');
    $comment_test = $page->find('css', '#comment-3 .field--name-comment-body')->getHtml();
    $expected = "<blockquote><p><strong>admin wrote:</strong> {$text}</p></blockquote><p>&nbsp;</p>";
    $this->assertSame($expected, $comment_test);

    // Test "comment quote selected".
    $script = <<<EndOfScript
(function ($) {
  let content = $('#comment-1 .field--name-comment-body').contents();
  let range = document.createRange();
  range.setStart(content[0].firstChild, 1);
  range.setEnd(content[0].firstChild, 10);
  let sel = document.getSelection();
  sel.removeAllRanges();
  sel.addRange(range);
})(jQuery);
EndOfScript;
    $session->evaluateScript($script);
    $this->click('.comment-quote-sel-link');
    $page->findButton('edit-submit')->click();
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->elementExists('css', '#comment-4');
    $comment_test = $page->find('css', '#comment-4 .field--name-comment-body')->getHtml();
    $expected = "<blockquote><p><strong>admin wrote:</strong> uis in er</p></blockquote><p>&nbsp;</p>";
    $this->assertSame($expected, $comment_test);
  }

}
