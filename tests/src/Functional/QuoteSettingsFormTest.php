<?php

namespace Drupal\Tests\quote\Functional;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\User;

/**
 * Tests settings form.
 *
 * @group quote
 */
class QuoteSettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'comment',
    'ckeditor5',
    'quote',
  ];

  /**
   * The configuration factory service.
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Settings form url.
   */
  protected Url $settingsRoute;

  /**
   * User with correct permissions.
   */
  protected User $user;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->user = $this->drupalCreateUser(['administer quote']);
    $this->configFactory = $this->container->get('config.factory');
    $this->settingsRoute = Url::fromRoute('quote.settings_form');
  }

  /**
   * Tests permissions to setting form.
   */
  public function testPermissionsToSettingsForm(): void {
    // Anonymous user.
    $this->drupalGet($this->settingsRoute);
    $this->assertSession()->statusCodeEquals(403);

    // Not admin user.
    $this->drupalLogin($this->drupalCreateUser());
    $this->drupalGet($this->settingsRoute);
    $this->assertSession()->statusCodeEquals(403);

    // Admin user.
    $this->drupalLogin($this->user);
    $this->drupalGet($this->settingsRoute);
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests settings save.
   */
  public function testSettingsSaving(): void {
    $types = ['page', 'article'];
    foreach ($types as $type_id) {
      $this->drupalCreateContentType(['type' => $type_id]);
    }

    $this->drupalLogin($this->user);
    $this->drupalGet($this->settingsRoute);

    $expected_values = $edit = [
      'quote_modes_quote_sel' => 1,
      'quote_modes_quote_all' => 1,
      'quote_modes_quote_reply_all' => 1,
      'quote_modes_quote_reply_sel' => 0,
      'quote_allow_comments' => 1,
      'quote_selector' => '#comment-form textarea',
      'quote_limit' => 400,
      'quote_selector_comment_quote_all' => '.field--name-comment-body',
      'quote_selector_node_quote_all' => '.field--name-body',
      'quote_ckeditor_support' => 1,
      'quote_html_tags_support' => 0,
    ];

    $allowed_types = \array_slice($types, 0);
    foreach ($allowed_types as $type_id) {
      $edit['quote_allow_types[' . $type_id . ']'] = $type_id;
      $expected_values['quote_allow_types'][$type_id] = $type_id;
    }

    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    foreach ($expected_values as $field => $expected_value) {
      $actual_value = $this->config('quote.settings')->get($field);
      $this->assertEquals($expected_value, $actual_value);
    }
  }

}
