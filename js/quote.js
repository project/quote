(function ($, Drupal, once) {
  Drupal.behaviors.quote = {
    attach(context) {
      const quoteLimit = drupalSettings.quote.quote_limit;
      const quoteCkeditor = drupalSettings.quote.quote_ckeditor_support;
      const quoteHtml = drupalSettings.quote.quote_html_tags_support;
      let editor = null;

      function getCkeditor() {
        if (quoteCkeditor && $('.ck-content').length) {
          editor = Drupal.CKEditor5Instances.get(
            Drupal.CKEditor5Instances.keys().next().value,
          );
        }
      }

      setTimeout(function () {
        getCkeditor();
      }, 500);

      function getSelectedText() {
        if (document.getSelection) {
          let selectedText = '';

          if (quoteHtml) {
            if (typeof window.getSelection !== 'undefined') {
              const sel = window.getSelection();
              if (sel.rangeCount) {
                const container = document.createElement('div');
                for (let i = 0, len = sel.rangeCount; i < len; ++i) {
                  container.appendChild(sel.getRangeAt(i).cloneContents());
                }
                selectedText = container.innerHTML;
              }
            } else if (typeof document.selection !== 'undefined') {
              if (document.selection.type === 'Text') {
                selectedText = document.selection.createRange().htmlText;
              }
            }
          } else {
            selectedText = document.getSelection().toString();
          }

          return selectedText.substring(0, quoteLimit);
        }
      }

      function getCommentArea() {
        return $(drupalSettings.quote.quote_selector);
      }

      function getCommentAreaCurValue(commentArea) {
        let curValue = commentArea.val();

        if (quoteCkeditor && $('.ck-content').length) {
          if (editor) {
            curValue = editor.getData();
          } else {
            getCkeditor();
            curValue = editor.getData();
          }
        }

        return curValue;
      }

      function setCommentAreaValue(commentArea, value) {
        commentArea.val(value);

        if (quoteCkeditor && $('.ck-content').length) {
          if (editor) {
            editor.setData(value);
          } else {
            getCkeditor();
            editor.setData(value);
          }
        }
      }

      $(once('comment-quote-sel', $('.comment-quote-sel-link'), context)).click(
        function (e) {
          e.preventDefault();
          const selected = getSelectedText();
          if (selected.length) {
            const commentArea = getCommentArea();
            const curValue = getCommentAreaCurValue(commentArea);
            const parent = $(this).closest('.comment');
            const username = parent.find('.comment__author a').text();
            const value = `${curValue}<blockquote><strong>${Drupal.t(
              '@author wrote:',
              { '@author': username },
            )}</strong> ${selected}</blockquote><p><br/></p>`;
            setCommentAreaValue(commentArea, value);
            // eslint-disable-next-line
            $('html, body').animate(
              {
                scrollTop: $('#comment-form').offset().top,
              },
              500,
            );
          }
        },
      );

      $(once('comment-quote-all', $('.comment-quote-all-link'), context)).click(
        function (e) {
          e.preventDefault();
          const commentArea = getCommentArea();
          const curValue = getCommentAreaCurValue(commentArea);
          const parent = $(this).closest('.comment');
          const username = parent.find('.comment__author a').text();
          let allText;

          if (quoteHtml) {
            allText = parent
              .find(drupalSettings.quote.quote_selector_comment_quote_all)
              .html()
              .substring(0, quoteLimit);
          } else {
            allText = parent
              .find(drupalSettings.quote.quote_selector_comment_quote_all)
              .text()
              .substring(0, quoteLimit);
          }

          const value = `${curValue}<blockquote><strong>${Drupal.t(
            '@author wrote:',
            { '@author': username },
          )}</strong> ${allText}</blockquote><p><br/></p>`;
          setCommentAreaValue(commentArea, value);
          // eslint-disable-next-line
          $('html, body').animate(
            {
              scrollTop: $('#comment-form').offset().top,
            },
            500,
          );
        },
      );

      $(once('node-quote-sel', $('.node-quote-sel-link'), context)).click(
        function (e) {
          e.preventDefault();
          const selected = getSelectedText();
          if (selected.length) {
            const commentArea = getCommentArea();
            const curValue = getCommentAreaCurValue(commentArea);
            const parent = $(this).closest('.node');
            const username = parent.find('.node__meta a').first().text();
            const value = `${curValue}<blockquote><strong>${Drupal.t(
              '@author wrote:',
              { '@author': username },
            )}</strong> ${selected}</blockquote><p><br/></p>`;
            setCommentAreaValue(commentArea, value);
            // eslint-disable-next-line
            $('html, body').animate(
              {
                scrollTop: $('#comment-form').offset().top,
              },
              500,
            );
          }
        },
      );

      $(once('node-quote-all', $('.node-quote-all-link'), context)).click(
        function (e) {
          e.preventDefault();
          const commentArea = getCommentArea();
          const curValue = getCommentAreaCurValue(commentArea);
          const parent = $(this).closest('.node');
          const username = parent.find('.node__meta a').first().text();
          let allText = '';

          if (quoteHtml) {
            allText = parent
              .find(drupalSettings.quote.quote_selector_node_quote_all)
              .html()
              .substring(0, quoteLimit);
          } else {
            allText = parent
              .find(drupalSettings.quote.quote_selector_node_quote_all)
              .text()
              .substring(0, quoteLimit);
          }

          const value = `${curValue}<blockquote><strong>${Drupal.t(
            '@author wrote:',
            { '@author': username },
          )}</strong> ${allText}</blockquote><p><br/></p>`;
          setCommentAreaValue(commentArea, value);
          // eslint-disable-next-line
          $('html, body').animate(
            {
              scrollTop: $('#comment-form').offset().top,
            },
            500,
          );
        },
      );
    },
  };
})(jQuery, Drupal, once);
